FROM ubuntu:18.04 as build
#FROM mhart/alpine-node as build

#RUN apk add --update curl && rm -rf /var/cache/apk/*
#RUN apk add bash
RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y nodejs build-essential git
RUN apt-get install -y  npm
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN curl https://install.meteor.com/ | sh
RUN node --version
RUN npm --version
